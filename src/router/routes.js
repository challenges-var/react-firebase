// IMPORT ROUTES
// import Home from '../views/Home';
// import About from '../views/About';
import Dashboard from '../components/dashboard/Dashboard';
import ProjectDetails from '../components/projects/ProjectDetails';
import SignIn from '../components/auth/SignIn';
import SignUp from '../components/auth/SignUp';
import CreateProject from '../components/projects/CreateProject';

// DEFINE ROUTE PROPERTIES IN ALPHABETICAL ORDER
const routes= [
  {
    id: 0,
    path: '/',
    component: Dashboard,
    exact: true
  },
  {
    id: 1,
    path: '/project/:id',
    component: ProjectDetails,
  },
  {
    id: 2,
    path: '/signin',
    component: SignIn,
  },
  {
    id: 3,
    path: '/signup',
    component: SignUp,
  },
  {
    id: 4,
    path: '/addproject',
    component: CreateProject,
  },
]

// EXPORT OBJECT
export default routes;