import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import routes from './routes';
import NavBar from '../components/layout/NavBar';

class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <NavBar />
          <div className="container">
            <Switch>
              {routes.map((route) => <Route key={route.id} {...route} />)}
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default Router;