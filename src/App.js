import React, { Component } from 'react';
import Router from './router';
import './styles/all.scss';

class App extends Component {
  render() {
    return (
      <Router />
    );
  }
}

export default App;
