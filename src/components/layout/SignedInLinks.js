import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { signOut } from '../../store/actions/authActions';

const SignedInLinks = (props) => {
  return (
    <ul className="right menu">
      <li className="yellow darken-4"><NavLink to='/addproject'><i className="large material-icons">add_box</i></NavLink></li>
      <li className="red darken-4"
        onClick={props.signOut}
      >
        <NavLink to='/logout'><i className="large material-icons">pause_circle_outline</i></NavLink>
      </li>
      <li><NavLink to='/' className='btn btn-floating pulse'>{props.profile.initials}</NavLink></li>
    </ul>
  )
}

// const mapStateToProps = (state) => {
//   return {
//     initials: state.firebase.profile.initials,
//   }
// }

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}


export default connect(null, mapDispatchToProps)(SignedInLinks);