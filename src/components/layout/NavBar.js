import React from 'react';
import { Link } from 'react-router-dom'
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import { connect } from 'react-redux'

const Navbar = (props) => {
  const { auth, profile } = props
  // console.log(auth)
  const links = auth.uid ? <SignedInLinks profile={profile} /> : <SignedOutLinks />;
  return (
    <nav className="nav-extended black">
      <div className="nav-wrapper ">
        <div className="container">
          <Link to='/' className="brand-logo center"><i className="large material-icons">nature_people</i></Link>
          <div className="nav-content">
            { links }
          </div>
        </div>
      </div>
    </nav>
  )
}

const mapStateToProps = (state) => {
  // console.log(state);
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
  }
}


export default connect(mapStateToProps)(Navbar);