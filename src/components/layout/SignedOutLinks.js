import React from 'react';
import { NavLink } from 'react-router-dom'

const SignedOutLinks = () => {
  return (
    <ul className="left menu">
      <li className="red darken-4"><NavLink to='/signup'><i className="large material-icons">blur_linear</i></NavLink></li>
      <li className="yellow darken-4"><NavLink to='/signin'><i className="large material-icons">play_circle_outline</i></NavLink></li>
    </ul>
  )
}
export default SignedOutLinks;