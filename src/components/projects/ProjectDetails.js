import React from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router-dom';
import moment from 'moment';


const ProjectDetails = (props) => {
  const { project, auth } = props;

  if(!auth.uid) return <Redirect to='/signin'/>
  if (project) {
    return (
      <div className="section project__details">
        <div className="card -z-depth-0">
          <div className="card-content">
            <span className="card-title">Project Title - {project.title}</span>
            <p>{project.content}</p>
          </div>
          <div className="card-action blue darken-4 white-text">
            <div>Posted By {project.authorFirstName} {project.authorLastName}</div>
            <div>{ moment(project.createdAt.toDate()).calendar() }</div>
          </div>
        </div>
      </div>
    )
  } else {
    return (
      <div className="container center">
        <h3>Loading projects...</h3>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  const projects = state.firestore.data.projects;
  // console.log(projects);
  const project = projects ? projects[id] : 'No project for the moment';
  return {
    project: project,
    auth: state.firebase.auth,
  }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([ { collection: 'projects' } ])
)(ProjectDetails);

