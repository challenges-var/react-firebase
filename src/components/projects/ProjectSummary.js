import React from 'react';
import moment from 'moment';


const ProjectSummary = ({project}) => {

  return (
    <div className="card z-depth-2 project__summary border-3">
      <div className="card-content grey-text text-darken-3 white border-3">
        <span className="card-title">{project.title}</span>
        <div className="card-action blue darken-4 white-text">
          <div>Posted By {project.authorFirstName} {project.authorLastName}</div>
          <div>{ moment(project.createdAt.toDate()).calendar() }</div>
        </div>
      </div>
    </div>
  )
}



export default ProjectSummary;