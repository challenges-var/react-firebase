  import firebase from 'firebase/app';
  import 'firebase/firestore';
  import 'firebase/auth';

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAY8jemaWE0fHEdaTi_UGR4KTSMkbfeTFY",
    authDomain: "reactonfire-a83c9.firebaseapp.com",
    databaseURL: "https://reactonfire-a83c9.firebaseio.com",
    projectId: "reactonfire-a83c9",
    storageBucket: "reactonfire-a83c9.appspot.com",
    messagingSenderId: "326597100845"
  };
  firebase.initializeApp(config);
  firebase.firestore().settings({ timestampsInSnapshots: true});

  export default firebase;